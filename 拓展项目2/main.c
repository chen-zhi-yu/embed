#include <iocc2530.h>
#include "hal_mcu.h"
#include "hal_assert.h"
#include "hal_board.h"
#include "hal_rf.h"
#include "basic_rf.h"
#include <stdio.h>
#include "info.h"
#include "uart0.h"
#include "htu21d.h"
#define RF_CHANNEL            12                                // 2.4 GHz RF channel
#define PAN_ID                0x2002
#define SEND_ADDR             0x1980
#define RECV_ADDR             0x2520

#define NODE_TYPE             0                              //0:接收节点，1:发送节点

static basicRfCfg_t basicRfConfig;
int recvCnt=0;
char recvBuf[30];
void collectNode(void){  
  char pTxData[30];  
  uint8 ret;
  int rlen;
  uint8 pRxData[128];
  static unsigned int send_counter = 0;                       //发送次数计数器
  // Keep Receiver off when not needed to save power
  basicRfReceiveOff();                                        //关闭射频接收器
  // Main loop
  
  while (TRUE) {
  while(1){
  unsigned char ch;
  ch = uart0_recv_char();				        //接收串口接收到的字节
  if (ch == '\0' || recvCnt >= 30) {                            //接收数据以@字符或者大于等于256结束
    recvBuf[recvCnt] = 0;
    break;
    recvCnt = 0;					        //收到数据清空
  } else {
    recvBuf[recvCnt++] = ch;				        //用数组储存接收到的数据
  }
  }
  for(int i=0;i<=30;i++){
  pTxData[i]=recvBuf[i];
  }
   
    ret = basicRfSendPacket(0xffff, (uint8*)pTxData, sizeof pTxData); 
    
    if (ret == SUCCESS) {
      basicRfReceiveOn();
      
      hal_led_on(1);
      halMcuWaitMs(100);
      hal_led_off(1);
      halMcuWaitMs(900);
      
      rlen = basicRfReceive(pRxData, sizeof pRxData, NULL);
      printf("send node send %d times\r\n",++send_counter);
      printf("rlen = %d \r\n",rlen);
      if(rlen > 0){
        pRxData[rlen] = 0;
        printf((char *)pRxData);
        printf("\r\n"); 
      }
    } else {
      hal_led_on(1);
      halMcuWaitMs(1000);
      hal_led_off(1);
      printf("send failed\r\n");
    }
  }
}

void endNode(void){
  //1.定义接收缓冲区
  uint8 pRxData[128];
  char pTxData[30];
  int rlen;
  uint8 ret;
  static unsigned int rec_counter = 0;
  basicRfReceiveOn(); 
  //2.判断是否有数据发送过来，没有就继续等待，如果有，则进行发送
   double data;                                        
    while (TRUE) {
      
      while(!basicRfPacketIsReady());
      rlen = basicRfReceive(pRxData, sizeof pRxData, NULL);
      if(rlen > 0) {       
        pRxData[rlen] = 0;
        printf((char *)pRxData);
        printf(" %d\r\n",++rec_counter);
        if(strcmp((char*)pRxData,"temp")==0){
          data=htu21d_get_data(TEMPERATURE)/100.0;
          sprintf(pTxData,"0x2521:温度:%.2f℃\r\n",data); 
          ret = basicRfSendPacket(0x1980, (uint8*)pTxData, sizeof pTxData);//广播发送数据包 
        }
        for(int i=0;i<=30;i++){
          pTxData[i]=0;
        }
        if (ret == SUCCESS) {
          hal_led_on(1);
          halMcuWaitMs(100);
          hal_led_off(1);
          halMcuWaitMs(900);
          printf("send OK!\r\n");
        } else {
          hal_led_on(1);
          halMcuWaitMs(1000);
          hal_led_off(1);
          printf("send ERR!\r\n");
        }
      }
    }
}

void main(void)
{
    halMcuInit();                                       
    htu21d_init();      //初始化mcu
    hal_led_init();
    //初始化LED 
#if NODE_TYPE
  uart0_init(0x00,0x00);
#else 
  hal_uart_init();                                            //初始化串口
#endif
    
    
    if (FAILED == halRfInit()) {                                //halRfInit()为初始化射频模块函数
        HAL_ASSERT(FALSE);
    }

    // Config basicRF
    basicRfConfig.panId = PAN_ID;                               //panID
    basicRfConfig.channel = RF_CHANNEL;                         //通信信道
    basicRfConfig.ackRequest = TRUE;                            //应答请求
#ifdef SECURITY_CCM
    basicRfConfig.securityKey = key;                            //安全秘钥
#endif

    
    // Initialize BasicRF
#if NODE_TYPE
    basicRfConfig.myAddr = SEND_ADDR;                           //发送地址
#else
    basicRfConfig.myAddr = RECV_ADDR;                           //接收地址
#endif
    
    if(basicRfInit(&basicRfConfig)==FAILED) {
      HAL_ASSERT(FALSE);
    }
#if NODE_TYPE
  collectNode();                                                 //发送数据
#else 
  endNode();                                                 //接收数据
#endif
}
