/*********************************************************************************************
* 文件：main.c
* 作者：Lixm 2017.10.17
* 说明：风扇实验 例程  
*       通过按键控制风扇开关，并将风扇的状态打印在PC上
* 修改：2017.11.08 chennian 将led、key文件替换为通用版驱动文件
*       2018.01.02 zhoucj   修改为串口一
* 注释： 
*********************************************************************************************/

/*********************************************************************************************
* 头文件
*********************************************************************************************/
#include <ioCC2530.h>

#include "delay.h"
#include "fan.h"
#include "led.h"
#include "uart1.h"
#include "key.h"
#include "stdio.h"
#include "string.h"
#include "timer.h"


/*********************************************************************************************
* 名称：main()
* 功能：逻辑代码
* 参数：无
* 返回：无
* 修改：
* 注释：
*********************************************************************************************/
 int flag=0;
u32 counter = 0;
void main(void)
{
                                    //风扇状态标志位
  unsigned short tick=0;
 
  
  
  xtal_init(); 
  time1_init();
  //系统时钟初始化     

  led_init();                                                   //LED初始化
  fan_init();                                                   //风扇初始化
  uart1_init(0x00,0x00);                                        //串口初始化
  key_init(); 
  //按键初始化
  
  while(1)
  {
   
     
      delay_ms(10);                                             //按键防抖
      
      if(K4 == DOWN)                                            //按键按下，改变2个LED灯状态
      {
          while(K4 == DOWN);                                      //松手检测
       
          flag=1;                                         //风扇状态标志位置1
          uart1_send_string("FAN ON,and Timer1 Start!\r\n");                     //串口打印提示信息
          FAN = FAN_ON;
         
          //开启风扇
       }
    
  }
    delay_ms(1);
    tick++;
    if(tick>999)
    {
        tick=0;
        LED5 = !LED5;
    }
  }
  
 
        
#pragma vector = T1_VECTOR
 __interrupt void T1_ISR(void)
  {
    
    EA=0;
   
    if(flag)
      counter++;   
       
      //统计进入中断的次数
     
    if(counter>3)                                               //初始化中定义10ms进一次中断，经过100次中断，10ms × 100 = 1S
    {
       counter=0;
    
       
        uart1_send_string("FAN OFF!\r\n");
          FAN = FAN_OFF;
      
          flag=0;
 
    }
    
    T1IF=0;    
    EA = 1;
  
   
  }

