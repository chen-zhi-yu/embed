#include <iocc2530.h>
#include "hal_mcu.h"
#include "hal_assert.h"
#include "hal_board.h"
#include "hal_rf.h"
#include "basic_rf.h"
#include <stdio.h>
#include "info.h"

#define RF_CHANNEL            11                                // 2.4 GHz RF channel
#define PAN_ID                0x2002
#define SEND_ADDR             0x2530
#define RECV_ADDR             0x2520
#define COMMAND               0x10

#define NODE_TYPE             0                                //0:接收节点，！0：发送节点

void io_init(void)
{
  P2SEL &= ~0x01;
  P2DIR &= ~0x01;
}

static basicRfCfg_t basicRfConfig;
int ledstatus = 0;
void rfSendData(void)
{
    uint8 pTxData[] = {COMMAND};
    uint8 key4;
     static unsigned char counter = 0;                         //记录K4被按下的次数
    // Keep Receiver off when not needed to save power
    basicRfReceiveOff();
    printf("{data=please press K4}");                          //显示提示信息
    key4 = P2_0;
   
    // Main loop
    while (TRUE) {
    
        halMcuWaitMs(100);
        hal_led_on(1);
        basicRfSendPacket(RECV_ADDR, pTxData, sizeof pTxData); 
         hal_led_off(1);
          halMcuWaitMs(100);
        printf("{data=K4 is pressed for %d times}",++counter);  //显示K4被按下
       
      }
   
    
    }


void rfRecvData(void)
{
  uint8 pRxData[128];
  int rlen;
   basicRfReceiveOn();
   printf("{data=LED is off}");                                 //显示LED初始状态              
    // Main loop
    while (TRUE) {
        while(!basicRfPacketIsReady());
        rlen = basicRfReceive(pRxData, sizeof pRxData, NULL);
        if(rlen > 0 && pRxData[0] == COMMAND) {
          if (ledstatus == 0) {
            hal_led_on(1);   
            ledstatus = 1;
            printf("{data=LED is on}");
          } else {
             hal_led_off(1);
             ledstatus = 0;
             printf("{data=LED is off}");
          }
        }
    }
}

void main(void)
{
    halMcuInit();

    io_init();
    
    hal_led_init();
    
    hal_uart_init();
    
    //lcd_dis();
    
    if (FAILED == halRfInit()) {
        HAL_ASSERT(FALSE);
    }

    // Config basicRF
    basicRfConfig.panId = PAN_ID;
    basicRfConfig.channel = RF_CHANNEL;
    basicRfConfig.ackRequest = TRUE;
#ifdef SECURITY_CCM
    basicRfConfig.securityKey = key;
#endif

    
    // Initialize BasicRF
#if NODE_TYPE
    basicRfConfig.myAddr = SEND_ADDR;
#else
    basicRfConfig.myAddr = RECV_ADDR; 
#endif
    
    if(basicRfInit(&basicRfConfig)==FAILED) {
      HAL_ASSERT(FALSE);
    }
#if NODE_TYPE
  rfSendData();
#else
  rfRecvData();   
#endif
}
